package main;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class App 
{
    @SuppressWarnings("all")
    public static void main( String[] args )
    {
        if (args.length < 2)
        {
            System.out.println("Missing arguments");
            return;
        }

        String pdfFile = args[0];
        String jsonFile = args[1];
        List<PDFTextFragment> fragments = new ArrayList<PDFTextFragment>();

        try
        {
            PDDocument doc = PDDocument.load(new File(pdfFile));
            PDFChunkStripper stripper = new PDFChunkStripper(fragments);
            stripper.getText(doc);
            doc.close();

            String json = makeJSON(fragments);
            Files.write(Paths.get(jsonFile), json.getBytes("UTF-8"));
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
            return;
        }

        System.out.println("SUCCESS");
    }


    @SuppressWarnings("all")
    static String makeJSON(List<PDFTextFragment> fragments)
    {
        JSONArray jsonArray = new JSONArray();
        JSONObject tmp;
        for (PDFTextFragment frag : fragments)
        {
            tmp = new JSONObject();
            tmp.put("Text", frag.Text);
            tmp.put("FontName", frag.FontName);
            tmp.put("FontSize", frag.FontSize);
            tmp.put("X", frag.X);
            tmp.put("Y", frag.Y);
            tmp.put("W", frag.W);
            tmp.put("H", frag.H);
            tmp.put("Page", frag.Page);
            jsonArray.add(tmp);
        }
        return jsonArray.toJSONString();
    }
}
