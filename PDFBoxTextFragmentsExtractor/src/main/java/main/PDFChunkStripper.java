package main;

import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

class PDFChunkStripper extends PDFTextStripper
{
    private List<PDFTextFragment> fragments;
    private int currentPage;

    public PDFChunkStripper(List<PDFTextFragment> fragments) throws IOException
    {
        this.fragments = fragments;
        currentPage = 1;
    }

    @Override
    public void processPage(PDPage page) throws IOException
    {
        super.processPage(page);
        currentPage++;
    }

    @Override
    public void writeString(String text, List<TextPosition> textPositions) throws IOException
    {
        super.writeString(text, textPositions);

        TextPosition tmp = (TextPosition)textPositions.get(0);
        String prevFontName = tmp.getFont().getName(), fontName;
        int prevFontSize = Math.round(tmp.getFontSize()), fontSize;

        String sub;
        int fragi = 0, i;
        TextPosition tmpPrev;
        for (i=1; i<textPositions.size(); i++)
        {
            tmp = (TextPosition)textPositions.get(i);
            tmpPrev = (TextPosition)textPositions.get(i-1);
            fontName = tmp.getFont().getName();
            fontSize = Math.round(tmp.getFontSize());
            if (fontName != prevFontName || fontSize != prevFontSize
            || Math.abs(tmp.getX() - (tmpPrev.getX() + tmpPrev.getWidth())) > 1)
            {
                sub = text.substring(fragi, i);
                makeFragment(sub, (TextPosition)textPositions.get(fragi), (TextPosition)textPositions.get(i - 1));
                prevFontName = fontName;
                prevFontSize = fontSize;
                fragi = i;
            }
        }
        sub = text.substring(fragi, i);
        makeFragment(sub, (TextPosition)textPositions.get(fragi), (TextPosition)textPositions.get(i - 1));
    }


    private void makeFragment(String text, TextPosition first, TextPosition last)
    {
        PDFTextFragment frag = new PDFTextFragment(
            text,
            first.getFont().getName(),
            Math.round(first.getFontSize()),
            Math.round(first.getX()),
            Math.round(first.getY()),
            Math.round((last.getX() - first.getX()) + last.getWidth()),
            Math.round(first.getHeight()),
            currentPage
            );
        fragments.add(frag);
    }
}