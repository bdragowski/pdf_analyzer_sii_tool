package main;

class PDFTextFragment
{
    public final String Text;
    public final String FontName;
    public final int FontSize;
    public final int X;
    public final int Y;
    public final int W;
    public final int H;
    public final int Page;

    public PDFTextFragment(String Text, String FontName, int FontSize, int X, int Y, int W, int H, int Page)
    {
        this.Text = Text;
        this.FontName = FontName;
        this.FontSize = FontSize;
        this.X = X;
        this.Y = Y;
        this.W = W;
        this.H = H;
        this.Page = Page;
    }
}