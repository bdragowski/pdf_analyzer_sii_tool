﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class VisualPageSearcher : Searcher
    {
        public enum Side
        {
            Left,
            Right,
            Top,
            Bottom,
            All
        }
        public interface IDistanceMetric
        {
            int Distance(TextFragment frag, TextFragment source);
            int Distance(TextFragment frag, int sourceX, int sourceY);
        }
        public class DefaultDistanceMetric : IDistanceMetric
        {
            public virtual int Distance(TextFragment frag, TextFragment source)
            { return Distance(frag, source.centerX, source.centerY); }

            public virtual int Distance(TextFragment frag, int sourceX, int sourceY)
            {
                int a = frag.centerX - sourceX;
                int b = frag.centerY - sourceY;
                int c = a * a + b * b;
                return (int)Math.Sqrt(c);
            }
        }
        public class LTRWeightedTaxiDistanceMetric : DefaultDistanceMetric
        {
            public override int Distance(TextFragment frag, int sourceX, int sourceY)
            {
                int a = frag.centerX - sourceX;
                if (a > 0) a *= 2; else a *= -1;
                int b = frag.centerY - sourceY;
                if (b > 0) b *= 2; else b *= -1;
                return a + b;
            }
        }

        public struct Result : IComparable<Result>
        {
            public TextParagraph Paragraph;
            public int Distance;

            public int CompareTo(Result other)
            {
                return Distance.CompareTo(other.Distance);
            }
        }

        private IDistanceMetric distanceMetric;
        private TextParagraph[] pageContent;

        public VisualPageSearcher(Page page) : base(page)
        {
            pageContent = page.AllParagraphs.ToArray();
            distanceMetric = new DefaultDistanceMetric();
        }
        public void SetDistanceMetric(IDistanceMetric metric)
        {
            distanceMetric = metric;
        }

        public List<Result> RadialSearch(ISearchCriteria criteria, TextFragment frag)
        {
            return RadialSearch(criteria, frag.centerX, frag.centerY);
        }
        public List<Result> RadialSearch(ISearchCriteria criteria, int x, int y)
        {
            //List<Result> results = new List<Result>();
            List<TextParagraph> paras = new List<TextParagraph>();
            TextParagraph para;
            for (int i=0; i<pageContent.Length; i++)
            {
                para = pageContent[i];
                if (criteria.Match(para))
                    paras.Add(para);
            }
            return MakeRadialResults(paras, x, y);
        }

        public List<Result> MakeRadialResults(List<TextParagraph> paras, TextFragment source)
        {
            return MakeRadialResults(paras, source.centerX, source.centerY);
        }
        public List<Result> MakeRadialResults(List<TextParagraph> paras, int sourceX, int sourceY)
        {
            List<Result> results = new List<Result>();
            foreach (var para in paras)
                results.Add(new Result() { Distance = distanceMetric.Distance(para, sourceX, sourceY), Paragraph = para });
            results.Sort();
            return results;
        }




        public List<Result> HorizontalSearch(ISearchCriteria criteria, TextFragment frag, Side side)
        {
            //List<Result> results = new List<Result>();
            List<TextParagraph> paras = new List<TextParagraph>();
            TextParagraph para;
            bool onLeft, onRight;
            for (int i = 0; i < pageContent.Length; i++)
            {
                para = pageContent[i];
                if ((para.Y <= frag.Y && para.endY > frag.Y)
                    || (para.Y <= frag.centerY && para.endY >= frag.centerY)
                    || (para.Y < frag.endY && para.endY >= frag.endY))
                {
                    onLeft = frag.centerX - para.centerX >= 0;
                    onRight = frag.centerX - para.centerX <= 0;

                    if ((onLeft && (side == Side.Left || side == Side.All))
                    || (onRight && (side == Side.Right || side == Side.All)))
                        if (criteria.Match(para))
                            paras.Add(para);
                }
            }
            return MakeHorizontalResults(paras, frag);
        }
        public List<Result> MakeHorizontalResults(List<TextParagraph> paras, TextFragment source)
        {
            return MakeHorizontalResults(paras, source.centerX, source.centerY);
        }
        public List<Result> MakeHorizontalResults(List<TextParagraph> paras, int sourceX, int sourceY)
        {
            List<Result> results = new List<Result>();
            foreach (var para in paras)
                results.Add(new Result() { Distance = sourceX - para.centerX, Paragraph = para });
            results.Sort();
            results.Reverse();
            return results;
        }



        public List<Result> VerticalSearch(ISearchCriteria criteria, TextFragment frag, Side side)
        {
            //List<Result> results = new List<Result>();
            List<TextParagraph> paras = new List<TextParagraph>();
            TextParagraph para;
            bool onTop, onBottom;
            for (int i = 0; i < pageContent.Length; i++)
            {
                para = pageContent[i];
                if ((para.X <= frag.X && para.endX > frag.X)
                    || (para.X <= frag.centerX && para.endX >= frag.centerX)
                    || (para.X < frag.endX && para.endX >= frag.endX))
                {
                    onTop = frag.centerY - para.centerY >= 0;
                    onBottom = frag.centerY - para.centerY <= 0;

                    if ((onTop && (side == Side.Top || side == Side.All))
                    || (onBottom && (side == Side.Bottom || side == Side.All)))
                        if (criteria.Match(para))
                            paras.Add(para);
                }
            }
            return MakeVerticalResults(paras, frag);
        }
        public List<Result> MakeVerticalResults(List<TextParagraph> paras, TextFragment source)
        {
            return MakeVerticalResults(paras, source.centerX, source.centerY);
        }
        public List<Result> MakeVerticalResults(List<TextParagraph> paras, int sourceX, int sourceY)
        {
            List<Result> results = new List<Result>();
            foreach (var para in paras)
                results.Add(new Result() { Distance = sourceY - para.centerY, Paragraph = para });
            results.Sort();
            results.Reverse();
            return results;
        }
    }
}
