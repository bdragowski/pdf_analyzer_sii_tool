﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public interface IDocumentManipulator
    {
        void Perform(TextParagraphGroup group);
        void Perform(Page page);
        void Perform(Document doc);
    }
}
