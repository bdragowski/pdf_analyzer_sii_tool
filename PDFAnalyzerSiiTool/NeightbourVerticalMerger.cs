﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class NeightbourVerticalMerger : ParagraphMerger
    {
        private int widthLimit;
        private int heightLimit;

        public NeightbourVerticalMerger(int widthLimit, int heightLimit)
        {
            this.widthLimit = widthLimit;
            this.heightLimit = heightLimit;
        }

        public override void MergeParagraphsIfCan(TextParagraphGroup group)
        {
            while (TryMergeNext(group.Paragraphs)) { };
        }

        private bool TryMergeNext(List<TextParagraph> paragraphs)
        {
            TextParagraph first, second;
            for (int i=0; i< paragraphs.Count; i++)
            {
                for (int j=0; j< paragraphs.Count; j++)
                {
                    if (j == i) continue;

                    first = paragraphs[i];
                    second = paragraphs[j];

                    if (!TextParagraph.HaveSameFont(first, second)) continue;

                    if (IsJustBelow(first, second))
                    {
                        if (IsAlignedLeft(first, second) || IsAlignedCenter(first, second) || IsAlignedRight(first, second))
                        {
                            first.Merge(second);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        private bool IsJustBelow(TextParagraph upper, TextParagraph lower)
        {
            return Math.Abs(upper.endY - lower.Y) <= heightLimit || Math.Abs(upper.endYFont - lower.Y) <= heightLimit/2;
        }

        private bool IsAlignedLeft(TextParagraph upper, TextParagraph lower)
        {
            return Math.Abs(upper.X - lower.X) <= widthLimit;
        }
        private bool IsAlignedCenter(TextParagraph upper, TextParagraph lower)
        {
            return Math.Abs(upper.centerX - lower.centerX) <= widthLimit;
        }
        private bool IsAlignedRight(TextParagraph upper, TextParagraph lower)
        {
            return Math.Abs(upper.endX - lower.endX) <= widthLimit;
        }
    }
}
