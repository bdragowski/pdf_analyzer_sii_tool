﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class CriteriaBuilder : ISearchCriteria
    {
        public enum Operator
        {
            AND,
            OR
        }
        private Operator opMode;

        private List<ISearchCriteria> criterias;
        public CriteriaBuilder(Operator opMode)
        {
            this.opMode = opMode;
            criterias = new List<ISearchCriteria>();
        }
        public CriteriaBuilder(ISearchCriteria criteria) : this(Operator.AND)
        { Add(criteria); }
        public CriteriaBuilder(Predicate<TextFragment> criteria) : this(Operator.AND)
        { Add(criteria); }
        public CriteriaBuilder() : this(Operator.AND)
        { }

        public void Add(ISearchCriteria criteria)
        {
            criterias.Add(criteria);
        }
        public void Add(Predicate<TextFragment> criteria)
        {
            Add(new SearchCritera(criteria));
        }

        public void Clear()
        {
            criterias.Clear();
        }

        public bool Match(TextFragment frag)
        {
            if (criterias.Count == 0)
                throw new InvalidOperationException("No criteria defined.");

            if (opMode == Operator.AND)
                return criterias.All(x => x.Match(frag));
            else if (opMode == Operator.OR)
                return criterias.Any(x => x.Match(frag));
            else
                throw new NotImplementedException("Unkown operator mode: " + opMode.ToString());
        }
    }






    public class CriteriaBuilderGroup : ISearchCriteriaGroup
    {
        private class PredicateWarpper : ISearchCriteriaGroup
        {
            private Predicate<TextParagraphGroup> criteria;
            public PredicateWarpper(Predicate<TextParagraphGroup> criteria) { this.criteria = criteria; }
            public bool Match(TextParagraphGroup frag) { return criteria.Invoke(frag); }
        }

        public enum CriteriaBuilderMode
        {
            ANY,
            ALL
        }
        private class SingleBuilderWrapper : ISearchCriteriaGroup
        {
            private CriteriaBuilder builder;
            private CriteriaBuilderMode mode;
            public SingleBuilderWrapper(CriteriaBuilder builder, CriteriaBuilderMode mode)
            { this.builder = builder; this.mode = mode; }
            public bool Match(TextParagraphGroup group)
            {
                if (mode == CriteriaBuilderMode.ANY)
                    return group.Paragraphs.Any(x => builder.Match(x));
                else if (mode == CriteriaBuilderMode.ALL)
                    return group.Paragraphs.All(x => builder.Match(x));
                else
                    throw new NotImplementedException("Unkown criteria builder mode: " + mode.ToString());
            }
        }


        public enum Operator
        {
            AND,
            OR
        }
        private Operator opMode;

        private List<ISearchCriteriaGroup> criterias;
        public CriteriaBuilderGroup(Operator opMode)
        {
            this.opMode = opMode;
            criterias = new List<ISearchCriteriaGroup>();
        }
        public CriteriaBuilderGroup(ISearchCriteriaGroup criteria) : this(Operator.AND)
        { Add(criteria); }
        public CriteriaBuilderGroup(Predicate<TextParagraphGroup> criteria) : this(Operator.AND)
        { Add(criteria); }
        public CriteriaBuilderGroup(CriteriaBuilder criteria) : this(Operator.AND)
        { Add(criteria); }
        public CriteriaBuilderGroup(CriteriaBuilder criteria, CriteriaBuilderMode mode) : this(Operator.AND)
        { Add(criteria, mode); }
        public CriteriaBuilderGroup() : this(Operator.AND)
        { }

        public void Add(ISearchCriteriaGroup criteria)
        {
            criterias.Add(criteria);
        }
        public void Add(Predicate<TextParagraphGroup> criteria)
        {
            Add(new PredicateWarpper(criteria));
        }
        public void Add(CriteriaBuilder criteria)
        {
            Add(criteria, CriteriaBuilderMode.ANY);
        }
        public void Add(CriteriaBuilder criteria, CriteriaBuilderMode mode)
        {
            Add(new SingleBuilderWrapper(criteria, mode));
        }

        public void Clear()
        {
            criterias.Clear();
        }

        public bool Match(TextParagraphGroup group)
        {
            if (criterias.Count == 0)
                throw new InvalidOperationException("No criteria defined.");

            if (opMode == Operator.AND)
                return criterias.All(x => x.Match(group));
            else if (opMode == Operator.OR)
                return criterias.Any(x => x.Match(group));
            else
                throw new NotImplementedException("Unkown operator mode: " + opMode.ToString());
        }
    }
}
