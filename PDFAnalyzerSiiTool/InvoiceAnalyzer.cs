﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class InvoiceAnalyzer
    {
        private Document document;
        private Searcher mainSearcher;

        public InvoiceAnalyzer(Document document)
        {
            this.document = document;
            mainSearcher = new Searcher(this.document);
        }


        public string FindInvoiceNumber()
        {
            // step 1 - find label introducing invoice number
            CriteriaBuilder criterias = new CriteriaBuilder();
            criterias.Add(x => x.ContainsAny(
                "faktury", "faktura",
                "dokument", "dokumentu",
                "numer",
                "invoice"));

            var labels = mainSearcher.SearchParagraphsAll(criterias, Searcher.NO_LIMIT);
            if (labels.Count == 0)
                throw new InvoiceException("No invoice number label found");

            // step 2 - find invoice number after label
            criterias.Clear();
            criterias.Add(x => x.CountDigits() >= 4);
            foreach (Searcher resultLabel in labels)
            {
                if (resultLabel.SearchWords(criterias, 8))
                    return resultLabel.Word.Text;
            }

            throw new InvoiceException("No invoice number found");
        }


        public void Dates()
        {
            Searcher datesSearcher = new Searcher(document.Pages[0]);

            CriteriaBuilder criterias = new CriteriaBuilder();
            //DateTime tmp;
            //criterias.Add(frag => DateTime.TryParse(frag.Text.Trim(), out tmp));
            criterias.Add(new IsDateCriteria());

            var dates = datesSearcher.SearchWordsAll(criterias, Searcher.NO_LIMIT);
            criterias.Clear();
            criterias.Add(x => x.ContainsAny(
                "termin", "data", "dnia",
                "date", "day"));
            Searcher rev;
            foreach (Searcher dateResult in dates)
            {
                rev = new ReversedSearcher(dateResult);
                if (rev.SearchWords(criterias, 8))
                    Console.WriteLine("{0}  :  {1}", rev.Paragraph.Text, dateResult.Paragraph.Text);
            }
        }
    }

    public class InvoiceException : Exception
    {
        public InvoiceException(string message) : base(message) { }
    }
}
