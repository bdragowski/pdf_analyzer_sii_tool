﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class Document
    {
        //public List<TextParagraph> AllParagraphs { get; private set; }
        public IEnumerable<TextParagraph> AllParagraphs
        {
            get
            {
                foreach (Page page in Pages)
                    foreach (TextParagraph para in page.AllParagraphs)
                        yield return para;
            }
        }
        public IEnumerable<TextParagraphGroup> AllParagraphGroups
        { 
            get
            {
                foreach (Page page in Pages)
                    foreach (TextParagraphGroup group in page.ParagraphGroups)
                        yield return group;
            }
        }
        public List<Page> Pages { get; private set; }

        public Document(List<TextParagraph> AllParagraphs)
        {
            //this.AllParagraphs = AllParagraphs;
            //this.AllParagraphs.ForEach(x => x.SetParentDocument(this));

            Pages = new List<Page>();
            Dictionary<int, List<TextParagraph>> pagesData = new Dictionary<int, List<TextParagraph>>();
            foreach (TextParagraph para in AllParagraphs)
            {
                if (!pagesData.ContainsKey(para.Page))
                    pagesData.Add(para.Page, new List<TextParagraph>());
                pagesData[para.Page].Add(para);
            }
            foreach (int ordin in pagesData.Keys)
                Pages.Add(new Page(ordin, pagesData[ordin]));
            Pages.ForEach(x => x.SetParentDocument(this));
        }

        internal Page NextPage(Page page)
        {
            int i;
            if ((i = Pages.IndexOf(page)) < Pages.Count - 1)
                return Pages[i + 1];
            else
                return null;
        }
        internal Page PreviousPage(Page page)
        {
            int i;
            if ((i = Pages.IndexOf(page)) > 0)
                return Pages[i - 1];
            else
                return null;
        }
    }
}
