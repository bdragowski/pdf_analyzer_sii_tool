﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PDFAnalyzerSiiTool
{
    public class JavaJSONPDFBoxExtractor : IPDFExtractor
    {
        private List<TextParagraph> currentFragments;
        private OrdinNumberCounter counter;

        public JavaJSONPDFBoxExtractor()
        {
            currentFragments = new List<TextParagraph>();
            counter = new OrdinNumberCounter();
        }

        public Document ExtractPDF(string filename)
        {
            currentFragments.Clear();
            counter.Reset();

            string jsonFile = Path.Combine(Inittializer.GetLocalDataDirectory(), "pdfresult.json");
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = Inittializer.JavaExecutable;

            StringBuilder args = new StringBuilder();
            args.Append("-jar");
            args.Append(" ");
            args.Append("\"" + Path.GetFullPath(Inittializer.PDFBoxJarExecutable) + "\"");
            args.Append(" ");
            args.Append("\"" + Path.GetFullPath(filename) + "\"");
            args.Append(" ");
            args.Append("\"" + jsonFile + "\"");

            psi.Arguments = args.ToString();
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;

            Process proc = Process.Start(psi);
            proc.WaitForExit();
            string output = proc.StandardOutput.ReadToEnd();

            
            if (output.Trim().Equals("SUCCESS"))
            {
                string json = File.ReadAllText(jsonFile);
                ParseJSON(json);
                File.Delete(jsonFile);
            }
            else
            {
                throw new Exception(output);
            }

            return new Document(currentFragments);
        }

        private void ParseJSON(string json)
        {
            dynamic data = JsonConvert.DeserializeObject(json);
            TextParagraph frag;
            foreach (dynamic item in data)
            {
                if (String.IsNullOrWhiteSpace(item.Text.Value))
                    continue;
                frag = new TextParagraph(
                    counter.Next(),
                    item.Text.Value.Trim(),
                    item.FontName.Value,
                    (int)item.FontSize.Value,
                    (int)item.X.Value,
                    (int)item.Y.Value,
                    (int)item.W.Value,
                    (int)item.H.Value,
                    (int)item.Page.Value
                    );
                currentFragments.Add(frag);
            }
        }
    }
}
