﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class Page
    {
        public Document ParentDocument { get; private set; }
        public Page PreviousPage { get { return ParentDocument.PreviousPage(this); } }
        public Page NextPage { get { return ParentDocument.NextPage(this); } }


        public int OrdinNumber { get; private set; }
        //public List<TextParagraph> AllParagraphs { get; private set; }
        public IEnumerable<TextParagraph> AllParagraphs
        {
            get
            {
                foreach (TextParagraphGroup group in ParagraphGroups)
                    foreach (TextParagraph para in group.Paragraphs)
                        yield return para;
            }
        }
        public List<TextParagraphGroup> ParagraphGroups { get; private set; }

        public Page(int OrdinNumber, List<TextParagraph> AllParagraphs)
        {
            this.OrdinNumber = OrdinNumber;
            //this.AllParagraphs = AllParagraphs;

            ParagraphGroups = new List<TextParagraphGroup>();
            ParagraphGroups.Add(new TextParagraphGroup(AllParagraphs, "Page " + OrdinNumber.ToString()));
            ParagraphGroups.ForEach(x => x.SetParentPage(this));
        }

        internal void SetParentDocument(Document doc)
        {
            ParentDocument = doc;
        }

        internal void SetGroups(List<TextParagraphGroup> ParagraphGroups)
        {
            this.ParagraphGroups = ParagraphGroups;
            this.ParagraphGroups.ForEach(x => x.SetParentPage(this));
        }
        internal void SplitGroup(TextParagraphGroup group, List<TextParagraphGroup> newGroups)
        {
            int index = ParagraphGroups.FindIndex(x => x == group);
            ParagraphGroups.RemoveAt(index);
            ParagraphGroups.InsertRange(index, newGroups);
            ParagraphGroups.ForEach(x => x.SetParentPage(this));
        }


        internal TextParagraphGroup NextGroup(TextParagraphGroup group)
        {
            int i;
            Page p;
            if ((i = ParagraphGroups.IndexOf(group)) < ParagraphGroups.Count - 1)
                return ParagraphGroups[i + 1];
            else
                return (p = NextPage) != null ? p.ParagraphGroups[0] : null;
        }
        internal TextParagraphGroup PreviousGroup(TextParagraphGroup group)
        {
            int i;
            Page p;
            if ((i = ParagraphGroups.IndexOf(group)) > 0)
                return ParagraphGroups[i - 1];
            else
                return (p = PreviousPage) != null ? p.ParagraphGroups[p.ParagraphGroups.Count - 1] : null;
        }
    }
}
