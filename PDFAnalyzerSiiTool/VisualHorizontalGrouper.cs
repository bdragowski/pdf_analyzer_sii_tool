﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class VisualHorizontalGrouper : ParagraphGrouper
    {
        private int spaceWeight;
        public VisualHorizontalGrouper(int spaceWeight)
        {
            this.spaceWeight = spaceWeight;
        }


        public override List<TextParagraphGroup> GroupParagraphs(List<TextParagraph> paragraphs, string prefix)
        {
            TextParagraph[] sorted = paragraphs.OrderBy(x => x.Y + x.H).ToArray();
            int height = sorted.Last().Y + sorted.Last().H;

            List<TextParagraphGroup> groups = new List<TextParagraphGroup>();
            List<TextParagraph> currentGroup = new List<TextParagraph>();
            string currentGroupName = prefix+"HGroup 0";

            int hline = 0;
            int emptyLines = 0;
            int startIndex = 0;
            int currentIndex = 0;
            for (; hline <= height; hline++)
            {
                if (IsLineEmpty(sorted, hline, ref currentIndex))
                {
                    if (++emptyLines >= spaceWeight)
                    {
                        if (currentIndex > startIndex)
                        {
                            for (int i=startIndex; i<currentIndex; i++)
                                currentGroup.Add(sorted[i]);
                            currentGroup.Sort();

                            startIndex = currentIndex;
                            groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));

                            currentGroup = new List<TextParagraph>();
                            currentGroupName = prefix+"HGroup " + hline.ToString();
                        }
                        emptyLines = 0;
                    }
                }
                else
                {
                    emptyLines = 0;
                }
            }

            if (sorted.Length > startIndex)
            {
                for (int i = startIndex; i < sorted.Length; i++)
                    currentGroup.Add(sorted[i]);
                currentGroup.Sort();
                groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));
            }
            return groups;
        }



        private bool IsLineEmpty(TextParagraph[] sorted, int line, ref int currentIndex)
        {
            TextParagraph para;
            for (; currentIndex < sorted.Length; currentIndex++)
            {
                para = sorted[currentIndex];
                if (para.Y > line)
                    return true;
                else if (para.Y <= line && para.Y + para.H >= line)
                    return false;
            }
            return true;
        }
    }
}
