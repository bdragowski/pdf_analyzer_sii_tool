﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class NeightbourHorizontalMerger : ParagraphMerger
    {
        private int widthLimit;
        private int heightLimit;

        public NeightbourHorizontalMerger(int widthLimit, int heightLimit)
        {
            this.widthLimit = widthLimit;
            this.heightLimit = heightLimit;
        }

        public override void MergeParagraphsIfCan(TextParagraphGroup group)
        {
            while (TryMergeNext(group.Paragraphs)) { };
        }

        private bool TryMergeNext(List<TextParagraph> paragraphs)
        {
            TextParagraph first, second;
            for (int i = 0; i < paragraphs.Count; i++)
            {
                for (int j = 0; j < paragraphs.Count; j++)
                {
                    if (j == i) continue;

                    first = paragraphs[i];
                    second = paragraphs[j];

                    if (!TextParagraph.HaveSameFont(first, second)) continue;

                    if (IsSameLine(first, second))
                    {
                        if (IsNeighbourRight(first, second))
                        {
                            first.Merge(second);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        private bool IsSameLine(TextParagraph first, TextParagraph second)
        {
            return Math.Abs(first.centerY - second.centerY) <= heightLimit;
        }

        private bool IsNeighbourRight(TextParagraph first, TextParagraph second)
        {
            return Math.Abs(first.endX - second.X) <= widthLimit /*|| Math.Abs((first.endX + first.FontSize/2) - second.X) <= widthLimit/2*/;
        }
    }
}
