﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PDFAnalyzerSiiTool
{
    public interface ISearchCriteria
    {
        bool Match(TextFragment frag);
    }
    public interface ISearchCriteriaGroup
    {
        bool Match(TextParagraphGroup group);
    }

    public class SearchCritera : ISearchCriteria
    {
        private Predicate<TextFragment> criteria;
        public SearchCritera(Predicate<TextFragment> criteria) { this.criteria = criteria; }
        public bool Match(TextFragment frag) { return criteria.Invoke(frag); }
    }


    public class IsDateCriteria : ISearchCriteria
    {
        private DateTime tmp;
        private string[] formats;// = { "d", "D" };
        private DateTimeFormatInfo dinfo;
        public IsDateCriteria()
        {
            dinfo = DateTimeFormatInfo.InvariantInfo;
            var formats = new List<string>();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
            {
                formats.AddRange(ci.DateTimeFormat.GetAllDateTimePatterns('d'));
                formats.AddRange(ci.DateTimeFormat.GetAllDateTimePatterns('D'));
            }
            this.formats = formats.Distinct().ToArray();
        }

        public bool Match(TextFragment frag)
        {
            return DateTime.TryParseExact(frag.EscapedGrammaCharsText(),
                formats,
                dinfo,
                DateTimeStyles.None,
                out tmp);
        }
        public DateTime GetMatchedDate()
        { return tmp; }
    }
    public class ContainsAnyCriteria : ISearchCriteria
    {
        private string[] keywords;
        public ContainsAnyCriteria(params string[] keywords) { this.keywords = keywords; }
        public bool Match(TextFragment frag) { return frag.ContainsAny(keywords); }
    }
    public class DoesNotContainsAnyCriteria : ISearchCriteria
    {
        private string[] keywords;
        public DoesNotContainsAnyCriteria(params string[] keywords) { this.keywords = keywords; }
        public bool Match(TextFragment frag) { return !frag.ContainsAny(keywords); }
    }
    public class EqualsAnyCriteria : ISearchCriteria
    {
        private string[] keywords;
        public EqualsAnyCriteria(params string[] keywords) { this.keywords = keywords; }
        public bool Match(TextFragment frag) { return frag.EqualsAny(keywords); }
    }


    public class ContainsMoneyAmountCriteria : ISearchCriteria
    {
        private decimal tmp;
        private bool withDecimals = true;

        public ContainsMoneyAmountCriteria() { }
        public ContainsMoneyAmountCriteria(bool withDecimals) { this.withDecimals = withDecimals; }

        public bool Match(TextFragment frag)
        {
            string numberParts = "";
            foreach(string part in frag.Text.Split(' '))
            {
                if (String.IsNullOrWhiteSpace(part)) continue;
                if (IsNumberPart(part))
                    numberParts += part;
                else if (numberParts.Length > 0)
                    break;
            }
            if (numberParts.Length < (withDecimals ? 4 : 3)) return false;

            if (withDecimals)
            {
                string last3 = numberParts.Substring(numberParts.Length - 3);
                // last three chars: no digit (, .), digit, digit
                if ((!IsDigit(last3[0]) && IsDigit(last3[1]) && IsDigit(last3[2])) == false)
                    return false;
            }

            string possibleNumber = numberParts;
            possibleNumber = possibleNumber.Replace(",", "");
            possibleNumber = possibleNumber.Replace(".", "");
            possibleNumber = possibleNumber.Replace(" ", "");

            bool success = Decimal.TryParse(possibleNumber, out tmp);
            if (success && withDecimals)
                tmp /= 100m;
            return success;
        }

        public decimal GetMatchedAmount()
        { return tmp; }

        private bool IsNumberPart(string part)
        {
            char first = part[0];
            char last = part[part.Length - 1];
            if (IsDigit(first) && IsDigit(last))
                return true;
            else
                return false;
        }
        private bool IsDigit(char ch)
        {
            return ch >= '0' && ch <= '9';
        }
    }

    public class RegexCriteria : ISearchCriteria
    {
        private Regex rx;
        private Match match;
        public RegexCriteria(string pattern)
        {
            rx = new Regex(pattern, RegexOptions.Compiled);
        }

        public bool Match(TextFragment frag)
        {
            match = rx.Match(frag.Text);
            return match != null && match.Success;
        }
        public Match GetLastMatch()
        { return match; }
    }
}
