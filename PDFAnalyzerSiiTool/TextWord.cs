﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class TextWord : TextFragment
    {
        public TextParagraph ParentParagraph { get; private set; }
        public TextWord PreviousWord { get { return ParentParagraph.PreviousWord(this); } }
        public TextWord NextWord { get { return ParentParagraph.NextWord(this); } }

        public int OrdinNumber { get; private set; }


        public override int X { get { return ParentParagraph.X + W*(OrdinNumber-1); } }
        public override int Y { get { return ParentParagraph.Y; } }
        public override int W { get { return ParentParagraph.W / ParentParagraph.Words.Count; } }
        public override int H { get { return ParentParagraph.H; } }

        public TextWord(TextParagraph ParentParagraph, int OrdinNumber, string Text)
        {
            this.ParentParagraph = ParentParagraph;
            this.OrdinNumber = OrdinNumber;
            this.Text = Text;
        }
    }
}
