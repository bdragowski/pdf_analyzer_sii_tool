﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class TextParagraph : TextFragment, IComparable<TextParagraph>
    {
        public TextParagraphGroup ParentGroup { get; private set; }
        public TextParagraph PreviousParagraph { get { return ParentGroup.PreviousParagraph(this); } }
        public TextParagraph NextParagraph { get { return ParentGroup.NextParagraph(this); } }


        public int OrdinNumber { get; private set; }
        public string FontName { get; private set; }
        public int FontSize { get; private set; }
        public int Page { get; private set; }


        public int endXFont { get { return X + W + FontSize; } }
        public int endYFont { get { return Y + FontSize; } }


        public List<TextWord> Words { get; private set; }
        public List<TextParagraph> MergedParagraphs { get; private set; }

        public TextParagraph(int OrdinNumber, string Text, string FontName, int FontSize, int X, int Y, int W, int H, int Page)
        {
            this.OrdinNumber = OrdinNumber;
            this.Text = Text.Replace((char)160, (char)32); // replace non-breaking spaces with normal spaces
            this.FontName = FontName;
            if (this.FontName.IndexOf('+') != -1)
                this.FontName = this.FontName.Substring(this.FontName.IndexOf('+')+1);
            this.FontSize = FontSize;
            this.X = X;
            this.Y = Y;
            this.W = W;
            this.H = H;
            this.Page = Page;

            MakeWords();

            MergedParagraphs = new List<TextParagraph>();
        }

        private void MakeWords()
        {
            Words = new List<TextWord>();
            OrdinNumberCounter counter = new OrdinNumberCounter();
            foreach (string word in Text.Split(' '))
            //foreach (string word in Text.Split(' ',',',':'))
            {
                if (!String.IsNullOrWhiteSpace(word))
                    Words.Add(new TextWord(this, counter.Next(), word.Trim()));
            }
            if (Words.Count == 0)
                Words.Add(new TextWord(this, counter.Next(), ""));
        }

        public void Merge(TextParagraph para)
        {
            int x = Math.Min(X, para.X);
            int y = Math.Min(Y, para.Y);
            int w = Math.Max(endX, para.endX) - x;
            int h = Math.Max(endY, para.endY) - y;

            X = x;
            Y = y;
            W = w;
            H = h;

            Text = Text.TrimEnd() + " " + para.Text.TrimStart();
            MakeWords();

            MergedParagraphs.Add(para);
            if (ParentGroup != null)
                ParentGroup.RemoveMergedParagraph(para);
        }

        /*
        internal void SetParentDocument(Document doc)
        {
            ParentDocument = doc;
        }
        */

        internal void SetParentGroup(TextParagraphGroup group)
        {
            ParentGroup = group;
        }



        internal TextWord NextWord(TextWord word)
        {
            int i;
            TextParagraph pr;
            if ((i = Words.IndexOf(word)) < Words.Count - 1)
                return Words[i + 1];
            else
                return (pr = NextParagraph) != null ? pr.Words[0] : null;
        }
        internal TextWord PreviousWord(TextWord word)
        {
            int i;
            TextParagraph pr;
            if ((i = Words.IndexOf(word)) > 0)
                return Words[i - 1];
            else
                return (pr = PreviousParagraph) != null ? pr.Words[pr.Words.Count - 1] : null;
        }




        public int CompareTo(TextParagraph other)
        {
            return OrdinNumber.CompareTo(other.OrdinNumber);
        }

        public static bool HaveSameFont(TextParagraph p1, TextParagraph p2)
        {
            return p1.FontName == p2.FontName && p1.FontSize == p2.FontSize;
        }

        public static List<TextParagraph> GetRange(TextParagraph from, TextParagraph to, bool inclusive)
        {
            List<TextParagraph> list = new List<TextParagraph>();
            if (inclusive)
                list.Add(from);
            if (from == to)
                return list;

            TextParagraph tmp = from.NextParagraph;
            for (;  tmp != to && tmp != null; tmp = tmp.NextParagraph)
                list.Add(tmp);

            if (tmp == null)
                throw new ArgumentException("Can't move from begin to end - possibly wrong order given?");

            if (inclusive)
                list.Add(to);
            return list;
        }
        public static List<TextParagraph> GetRange(TextParagraph from, TextParagraph to)
        {
            return GetRange(from, to, true);
        }
    }
}
