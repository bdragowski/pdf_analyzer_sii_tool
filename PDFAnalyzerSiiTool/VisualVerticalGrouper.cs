﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class VisualVerticalGrouper : ParagraphGrouper
    {
        private int spaceWeight;
        public VisualVerticalGrouper(int spaceWeight)
        {
            this.spaceWeight = spaceWeight;
        }


        public override List<TextParagraphGroup> GroupParagraphs(List<TextParagraph> paragraphs, string prefix)
        {
            TextParagraph[] sorted = paragraphs.OrderBy(x => x.X + x.W).ToArray();
            int width = sorted.Last().X + sorted.Last().W;

            List<TextParagraphGroup> groups = new List<TextParagraphGroup>();
            List<TextParagraph> currentGroup = new List<TextParagraph>();
            string currentGroupName = prefix + "VGroup 0";

            int hline = 0;
            int emptyLines = 0;
            int startIndex = 0;
            int currentIndex = 0;
            for (; hline <= width; hline++)
            {
                if (IsLineEmpty(sorted, hline, ref currentIndex))
                {
                    if (++emptyLines >= spaceWeight)
                    {
                        if (currentIndex > startIndex)
                        {
                            for (int i = startIndex; i < currentIndex; i++)
                                currentGroup.Add(sorted[i]);
                            currentGroup.Sort();

                            startIndex = currentIndex;
                            groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));

                            currentGroup = new List<TextParagraph>();
                            currentGroupName = prefix + "VGroup " + hline.ToString();
                        }
                        emptyLines = 0;
                    }
                }
                else
                {
                    emptyLines = 0;
                }
            }

            if (sorted.Length > startIndex)
            {
                for (int i = startIndex; i < sorted.Length; i++)
                    currentGroup.Add(sorted[i]);
                currentGroup.Sort();
                groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));
            }
            return groups;
        }



        private bool IsLineEmpty(TextParagraph[] sorted, int line, ref int currentIndex)
        {
            TextParagraph para;
            for (; currentIndex < sorted.Length; currentIndex++)
            {
                para = sorted[currentIndex];
                if (para.X > line)
                    return true;
                else if (para.X <= line && para.X + para.W >= line)
                    return false;
            }
            return true;
        }
    }
}
