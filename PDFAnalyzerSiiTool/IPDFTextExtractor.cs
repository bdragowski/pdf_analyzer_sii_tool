﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public interface IPDFExtractor
    {
        Document ExtractPDF(string filename);
    }
}
