﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class OrdinNumberCounter
    {
        private int current = 0;

        public int Next()
        {
            return ++current;
        }
        public void Reset()
        {
            current = 0;
        }
    }
}
