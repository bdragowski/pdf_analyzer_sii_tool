﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class DifferentFontGrouper : ParagraphGrouper
    {

        public override List<TextParagraphGroup> GroupParagraphs(List<TextParagraph> paragraphs, string prefix)
        {
            TextParagraph[] paras = paragraphs.ToArray();

            List<TextParagraphGroup> groups = new List<TextParagraphGroup>();
            List<TextParagraph> currentGroup = new List<TextParagraph>();
            int groupindex = 0;
            string currentGroupName = prefix + "FGroup " + groupindex++.ToString();

            int startIndex = 0;
            int currentIndex = 0;
            for (; currentIndex < paras.Length; currentIndex++)
            {
                if (currentIndex == 0) continue;

                if (!TextParagraph.HaveSameFont(paras[currentIndex], paras[currentIndex-1]))
                {
                    for (int i = startIndex; i < currentIndex; i++)
                        currentGroup.Add(paras[i]);

                    startIndex = currentIndex;
                    groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));

                    currentGroup = new List<TextParagraph>();
                    currentGroupName = prefix + "FGroup " + groupindex++.ToString();
                }
            }

            if (paras.Length > startIndex)
            {
                for (int i = startIndex; i < paras.Length; i++)
                    currentGroup.Add(paras[i]);
                currentGroup.Sort();
                groups.Add(new TextParagraphGroup(currentGroup, currentGroupName));
            }

            return groups;
        }
    }
}
