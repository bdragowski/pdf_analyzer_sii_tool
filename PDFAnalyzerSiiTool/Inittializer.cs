﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public static class Inittializer
    {
        public static string JavaExecutable { get; private set; }
        public static string PDFBoxJarExecutable { get; private set; }

        static Inittializer()
        {
            Initialize();
        }

        private static bool isInitialized = false;
        public static void Initialize()
        {
            Initialize(false);
        }
        public static void Initialize(bool force)
        {
            if (!isInitialized || force)
            {
                FindJavaExecutable();
                ExtractPDFBoxJar();
            }
            isInitialized = true;
        }


        public static string GetLocalDataDirectory()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, "PDFAnalyzerSiiTool");
            Directory.CreateDirectory(path);
            return path;
        }


        private static void FindJavaExecutable()
        {
            const string JAVA_KEY = "SOFTWARE\\JavaSoft\\Java Runtime Environment\\";
            JavaExecutable = null;

            string environmentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
            if (!string.IsNullOrEmpty(environmentPath))
            {
                JavaExecutable = Path.Combine(environmentPath, "bin\\java.exe");
                if (File.Exists(JavaExecutable))
                    return;
                else
                    JavaExecutable = null;
            }

            var localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            using (var rk = localKey.OpenSubKey(JAVA_KEY))
            {
                if (rk != null)
                {
                    string currentVersion = rk.GetValue("CurrentVersion").ToString();
                    using (var key = rk.OpenSubKey(currentVersion))
                    {
                        JavaExecutable = Path.Combine(key.GetValue("JavaHome").ToString(), "bin\\java.exe");
                        if (File.Exists(JavaExecutable))
                            return;
                        else
                            JavaExecutable = null;
                    }
                }
            }

            string programFiles = Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles);
            string javaDir = Path.Combine(programFiles, "java");
            if (Directory.Exists(javaDir))
            {
                foreach (string javaSubDir in Directory.GetDirectories(javaDir))
                {
                    JavaExecutable = Path.Combine(javaSubDir, "bin\\java.exe");
                    if (File.Exists(JavaExecutable))
                        return;
                    else
                        JavaExecutable = null;
                }
            }

            throw new SystemException("Couldn't find java executable.");
        }
        private static void ExtractPDFBoxJar()
        {
            const string filename = "PDFBoxTextFragmentsExtractor.jar";

            PDFBoxJarExecutable = Path.Combine(GetLocalDataDirectory(), filename);

            if (File.Exists(PDFBoxJarExecutable))
            {
                FileInfo jarInfo = new FileInfo(PDFBoxJarExecutable);
                FileInfo myInfo = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                if (jarInfo.Length == Properties.Resource.PDFBoxTextFragmentsExtractor_1_0.Length
                    && jarInfo.LastWriteTime > myInfo.LastWriteTime)
                    return;
                else
                    File.Delete(PDFBoxJarExecutable);
            }

            File.WriteAllBytes(PDFBoxJarExecutable, Properties.Resource.PDFBoxTextFragmentsExtractor_1_0);

            if (!File.Exists(PDFBoxJarExecutable))
                throw new SystemException("Couldn't extract java PDFBox jar tool.");
        }
    }
}
