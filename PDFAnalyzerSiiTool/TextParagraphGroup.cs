﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class TextParagraphGroup
    {
        public Page ParentPage { get; private set; }
        public TextParagraphGroup PreviousGroup { get { return ParentPage.PreviousGroup(this); } }
        public TextParagraphGroup NextGroup { get { return ParentPage.NextGroup(this); } }

        public string Name { get; private set; }
        public List<TextParagraph> Paragraphs { get; private set; }

        public TextParagraphGroup(List<TextParagraph> Paragraphs, string Name)
        {
            this.Name = Name;
            this.Paragraphs = Paragraphs;
            this.Paragraphs.ForEach(x => x.SetParentGroup(this));
        }

        internal void SetParentPage(Page page)
        {
            ParentPage = page;
        }
        internal void RemoveMergedParagraph(TextParagraph merged)
        {
            Paragraphs.Remove(merged);
        }

        internal TextParagraph NextParagraph(TextParagraph paragraph)
        {
            int i;
            TextParagraphGroup g;
            if ((i = Paragraphs.IndexOf(paragraph)) < Paragraphs.Count - 1)
                return Paragraphs[i + 1];
            else
                return (g = NextGroup) != null ? g.Paragraphs[0] : null;
        }
        internal TextParagraph PreviousParagraph(TextParagraph paragraph)
        {
            int i;
            TextParagraphGroup g;
            if ((i = Paragraphs.IndexOf(paragraph)) > 0)
                return Paragraphs[i - 1];
            else
                return (g = PreviousGroup) != null ? g.Paragraphs[g.Paragraphs.Count - 1] : null;
        }


        public static List<TextParagraphGroup> GetRange(TextParagraphGroup from, TextParagraphGroup to, bool inclusive)
        {
            List<TextParagraphGroup> list = new List<TextParagraphGroup>();
            if (inclusive)
                list.Add(from);
            if (from == to)
                return list;

            TextParagraphGroup tmp = from.NextGroup;
            for (; tmp != to && tmp != null; tmp = tmp.NextGroup)
                list.Add(tmp);

            if (tmp == null)
                throw new ArgumentException("Can't move from begin to end - possibly wrong order given?");

            if (inclusive)
                list.Add(to);
            return list;
        }
        public static List<TextParagraphGroup> GetRange(TextParagraphGroup from, TextParagraphGroup to)
        {
            return GetRange(from, to, true);
        }
    }
}
