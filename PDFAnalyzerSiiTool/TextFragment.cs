﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public abstract class TextFragment
    {
        public string Text { get; protected set; }

        public virtual int X { get; protected set; }
        public virtual int Y { get; protected set; }
        public virtual int W { get; protected set; }
        public virtual int H { get; protected set; }


        public virtual int endX { get { return X + W; } }
        public virtual int endY { get { return Y + H; } }
        public virtual int centerX { get { return X + W / 2; } }
        public virtual int centerY { get { return Y + H / 2; } }

        public int Length()
        {
            return Text.Length;
        }


        public string EscapedGrammaCharsText()
        {
            return Text.Trim(',', ':');
        }
 

        public int CountDigits()
        {
            int digits = 0;
            foreach(char c in Text)
            {
                if (c >= '0' && c <= '9')
                    digits++;
            }
            return digits;
        }

        public bool ContainsAny(params string[] phrases)
        {
            foreach (string phrase in phrases)
            {
                if (Text.IndexOf(phrase, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
            }
            return false;
        }
        public bool EqualsAny(params string[] phrases)
        {
            foreach (string phrase in phrases)
            {
                if (Text.Equals(phrase, StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
