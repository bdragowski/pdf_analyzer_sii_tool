﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public class Searcher
    {
        public const int NO_LIMIT = 0;
        public static ISearchCriteria NO_CRITERIA
        {
            get { return new SearchCritera(x => true); }
        }

        public struct Limit
        {
            public Page page;
            public TextParagraphGroup group;
        }
        public Limit limit { get; protected set; }

        protected Document doc;

        protected int pageIndex = 0;
        protected int groupIndex = 0;
        protected int paragraphIndex = 0;
        protected int wordIndex = 0;

        public Document Document { get { return doc; } }
        public Page Page { get { return doc.Pages[pageIndex]; } }
        public TextParagraphGroup Group { get { return doc.Pages[pageIndex].ParagraphGroups[groupIndex]; } }
        public TextParagraph Paragraph { get { return doc.Pages[pageIndex].ParagraphGroups[groupIndex].Paragraphs[paragraphIndex]; } }
        public TextWord Word { get { return doc.Pages[pageIndex].ParagraphGroups[groupIndex].Paragraphs[paragraphIndex].Words[wordIndex]; } }
        //public int PageNumber { get { return data[paragraphIndex].Page; } }


        public Searcher (Searcher orig)
        {
            this.doc = orig.doc;
            this.limit = orig.limit;

            this.pageIndex = orig.pageIndex;
            this.groupIndex = orig.groupIndex;
            this.paragraphIndex = orig.paragraphIndex;
            this.wordIndex = orig.wordIndex;
    }
        public Searcher(Document doc)
        {
            this.doc = doc;
            limit = new Limit() { group = null, page = null };
        }
        public Searcher(Page page)
        {
            doc = page.ParentDocument;
            limit = new Limit() { group = null, page = page };
            while (Page != page)
            {
                if (!NextPage())
                    throw new ArgumentException("Can't find given page in parent document");
            }
        }
        public Searcher(TextParagraphGroup group)
        {
            doc = group.ParentPage.ParentDocument;
            limit = new Limit() { group = group, page = null };
            while (Group != group)
            {
                if (!NextGroup())
                    throw new ArgumentException("Can't find given group in parent document");
            }
        }

        /*
        public Searcher(List<TextParagraph> data)
        {
            this.data = data;
        }
        */



        public bool SearchParagraphs(ISearchCriteria criterias, int limit)
        {
            int moves = 0;
            do {
                if (criterias.Match(Paragraph))
                    return true;
            } while ((++moves < limit || limit == NO_LIMIT) && NextParagraph());
            return false;
        }
        public List<Searcher> SearchParagraphsAll(ISearchCriteria criterias, int limit)
        {
            List<Searcher> matches = new List<Searcher>();
            int moves = 0;
            do {
                if (criterias.Match(Paragraph))
                    matches.Add(Clone());
            } while ((++moves < limit || limit == NO_LIMIT) && NextParagraph());
            return matches;
        }


        public bool SearchWords(ISearchCriteria criterias, int limit)
        {
            int moves = 0;
            do {
                if (criterias.Match(Word))
                    return true;
            } while ((++moves < limit || limit == NO_LIMIT) && NextWord());
            return false;
        }
        public List<Searcher> SearchWordsAll(ISearchCriteria criterias, int limit)
        {
            List<Searcher> matches = new List<Searcher>();
            int moves = 0;
            do
            {
                if (criterias.Match(Word))
                    matches.Add(Clone());
            } while ((++moves < limit || limit == NO_LIMIT) && NextWord());
            return matches;
        }



        public bool SearchGroups(ISearchCriteriaGroup criterias, int limit)
        {
            int moves = 0;
            do
            {
                if (criterias.Match(Group))
                    return true;
            } while ((++moves < limit || limit == NO_LIMIT) && NextGroup());
            return false;
        }
        public List<Searcher> SearchGroupsAll(ISearchCriteriaGroup criterias, int limit)
        {
            List<Searcher> matches = new List<Searcher>();
            int moves = 0;
            do
            {
                if (criterias.Match(Group))
                    matches.Add(Clone());
            } while ((++moves < limit || limit == NO_LIMIT) && NextGroup());
            return matches;
        }



        public virtual bool NextPage()
        {
            if (pageIndex == Document.Pages.Count - 1)
                return false;

            pageIndex++;
            groupIndex = 0;
            paragraphIndex = 0;
            wordIndex = 0;
            return true;
        }
        public virtual bool NextGroup()
        {
            if (groupIndex == Page.ParagraphGroups.Count - 1)
            {
                if (limit.page == null)
                    return NextPage();
                else
                    return false;
            }

            groupIndex++;
            paragraphIndex = 0;
            wordIndex = 0;
            return true;
        }
        public virtual bool NextParagraph()
        {
            if (paragraphIndex == Group.Paragraphs.Count-1)
            {
                if (limit.group == null)
                    return NextGroup();
                else
                    return false;
            }
    
            paragraphIndex++;
            wordIndex = 0;
            return true;
        }
        public virtual bool NextWord()
        {
            if (wordIndex == Paragraph.Words.Count-1)
                return NextParagraph();

            wordIndex++;
            return true;
        }


        public void Reset()
        {
            paragraphIndex = 0;
            wordIndex = 0;
            if (limit.group == null)
                groupIndex = 0;
            if (limit.group == null && limit.page == null)
                pageIndex = 0;
        }


        public Searcher Clone()
        {
            return (Searcher)MemberwiseClone();
        }
    }


    public class ReversedSearcher : Searcher
    {
        public ReversedSearcher(Searcher baseSearcher) : base(baseSearcher) {}

        public override bool NextPage()
        {
            if (pageIndex == 0)
                return false;

            pageIndex--;
            groupIndex = Page.ParagraphGroups.Count - 1;
            paragraphIndex = Group.Paragraphs.Count - 1;
            wordIndex = Paragraph.Words.Count - 1;
            return true;
        }
        public override bool NextGroup()
        {
            if (groupIndex == 0)
            {
                if (limit.page == null)
                    return NextPage();
                else
                    return false;
            }

            groupIndex--;
            paragraphIndex = Group.Paragraphs.Count - 1;
            wordIndex = Paragraph.Words.Count - 1;
            return true;
        }
        public override bool NextParagraph()
        {
            if (paragraphIndex == 0)
            {
                if (limit.group == null)
                    return NextGroup();
                else
                    return false;
            }

            paragraphIndex--;
            wordIndex = Paragraph.Words.Count - 1;
            return true;
        }
        public override bool NextWord()
        {
            if (wordIndex == 0)
                return NextParagraph();

            wordIndex--;
            return true;
        }
    }
}
