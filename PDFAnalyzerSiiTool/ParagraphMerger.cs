﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public abstract class ParagraphMerger : IDocumentManipulator
    {
        public abstract void MergeParagraphsIfCan(TextParagraphGroup group);
        public virtual void MergeParagraphsIfCan(Page page)
        {
            foreach (TextParagraphGroup group in page.ParagraphGroups)
                MergeParagraphsIfCan(group);
        }
        public virtual void MergeParagraphsIfCan(Document doc)
        {
            foreach (Page page in doc.Pages)
                MergeParagraphsIfCan(page);
        }


        void IDocumentManipulator.Perform(TextParagraphGroup group)
        {
            MergeParagraphsIfCan(group);
        }
        void IDocumentManipulator.Perform(Page page)
        {
            MergeParagraphsIfCan(page);
        }
        void IDocumentManipulator.Perform(Document doc)
        {
            MergeParagraphsIfCan(doc);
        }
    }
}
