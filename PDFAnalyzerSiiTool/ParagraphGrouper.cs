﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFAnalyzerSiiTool
{
    public abstract class ParagraphGrouper : IDocumentManipulator
    {
        public abstract List<TextParagraphGroup> GroupParagraphs(List<TextParagraph> paragraphs, string prefix);
        public virtual List<TextParagraphGroup> GroupParagraphs(List<TextParagraph> paragraphs)
        {
            return GroupParagraphs(paragraphs, "");
        }
        public virtual List<TextParagraphGroup> GroupParagraphs(TextParagraphGroup group)
        {
            var data = GroupParagraphs(group.Paragraphs, group.Name+" - ");
            if (group.ParentPage != null)
                group.ParentPage.SplitGroup(group, data);
            return data;
        }
        public virtual void GroupParagraphs(Page page)
        {
            List<TextParagraphGroup> newGroups = new List<TextParagraphGroup>();
            foreach (TextParagraphGroup oldGroup in page.ParagraphGroups)
                newGroups.AddRange(GroupParagraphs(oldGroup.Paragraphs, oldGroup.Name+" - "));

            page.SetGroups(newGroups);
        }
        public virtual void GroupParagraphs(Document doc)
        {
            foreach (Page page in doc.Pages)
                GroupParagraphs(page);
        }



        void IDocumentManipulator.Perform(TextParagraphGroup group)
        {
            GroupParagraphs(group);
        }
        void IDocumentManipulator.Perform(Page page)
        {
            GroupParagraphs(page);
        }
        void IDocumentManipulator.Perform(Document doc)
        {
            GroupParagraphs(doc);
        }
    }
}
